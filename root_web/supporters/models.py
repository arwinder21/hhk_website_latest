from django.db import models
from django.utils import timezone


class supporter(models.Model):
    name = models.CharField(max_length=100,blank=False)
    age = models.CharField(max_length=100,null=True,blank=True)
    gender = models.CharField(max_length=15, null=True)
    phone_no = models.CharField(max_length=20,unique=True, blank=False)
    alt_phone_no = models.CharField(max_length=20, null=True)
    email_id = models.EmailField(null=True)
    city = models.CharField(max_length=100, null=True)
    Reference = models.CharField(max_length=100, null=True)
    pan_card = models.CharField(max_length=100, null=True)
    address_a = models.CharField(max_length=100, null=True)
    address_b = models.CharField(max_length=100, null=True)
    address_c = models.CharField(max_length=100, null=True)
    is_constant = models.BooleanField(default=False, null=True)
    amount = models.IntegerField(default=0,null=True)
    is_sponsor = models.BooleanField(default=False, null=True)
    sponsee = models.CharField(max_length=300, null=True)
    is_one_time = models.BooleanField(default=False, null=True)
    resources = models.BooleanField(default=False)
    is_si = models.BooleanField(default = False)
    si_date = models.IntegerField(null = True)
    dob = models.DateField(null = True,blank=True)
    profession = models.CharField(max_length= 200,null = True)
    is_verified = models.BooleanField(default=False)
    last_generated = models.IntegerField(default=0)
    date_added = models.DateTimeField(default=timezone.now)
    date_edited = models.DateTimeField(default=timezone.now)

    def update_added(self):
        self.last_generated = int(timezone.localdate().month)

    def __str__(self):
        return str([self.name,self.phone_no])
