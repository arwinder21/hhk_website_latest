from reportlab.pdfgen import canvas

from num2words import num2words
def hello(f_name,rec_no,name,amount,mode,date):
    c = canvas.Canvas(f_name)
    c.drawInlineImage("donform.jpeg", 40, 20,510,770)
    c.drawString(75,637,rec_no)
    c.drawString(75, 530, name)
    #c.drawString(475, 637, date_rec)
    c.drawString(265, 492, amount)
    c.drawString(170, 474, (str(num2words(amount))).capitalize())
    c.drawString(320, 318, 'U80211PB2017NPL046830')
    c.drawString(320, 285, 'AAECH2743L')
    c.drawString(320, 255, '8437833465')
    c.drawString(95, 318, mode)
    c.drawString(95, 284, date)
    c.save()

#hello("abc.pdf","21","hhhh","210","Cash","21-21-1212")