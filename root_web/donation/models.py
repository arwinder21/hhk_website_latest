from django.db import models
# Create your models here.
mode_choces = [
    ('CASH', 'Cash'),
    ('INET', 'Internet Banking'),
    ('UPI', 'UPI'),
    ('IMPS', 'IMPS'),
    ('NEFT', 'NEFT'),
    ('CHEQUE', 'Cheque'),
]

class verified_donation(models.Model):
    rec_no = models.AutoField(primary_key=True)
    recipt_num = models.IntegerField(default = 0)
    member = models.ForeignKey('supporters.supporter', on_delete=models.CASCADE,null=True)
    anon_name = models.CharField(max_length = 100,null = True,blank =True)
    amount = models.CharField(max_length = 20, blank=False)
    date_donation = models.DateField(blank=False)
    mode_donation = models.CharField(max_length = 100,default = 'CASH',choices = mode_choces)
    date_rec = models.DateField()
    migrated = models.BooleanField(default = False)
    anon = models.BooleanField(default = False)
    mail_sent = models.BooleanField(default = False)


class misc(models.Model):
    rec_no_prefix = models.IntegerField(default = 0)