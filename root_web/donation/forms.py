from django.forms import ModelForm,CharField,DateField,Form,FileField
from .models import verified_donation,misc


class IndonForm(ModelForm):
    class Meta:
        model = verified_donation
        fields = ['amount','mode_donation']


class AnondonForm(ModelForm):
    amount = CharField(required=True)
    class Meta:
        model = verified_donation
        fields = ['anon_name','amount','mode_donation']


class miscForm(ModelForm):
    class Meta:
        model = misc
        fields = ['rec_no_prefix']
    


class Uploadfiles(Form):
    file = FileField()